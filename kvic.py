import datetime
import scrapy

from ..items import OrganizationItem, CourseItem, ItemMetadata


class KVICSpider(scrapy.Spider):
    name = 'kvic'

    # site offers query of all courses (e.g. 1000000 or other absurdly high number to ensure all courses) on one page
    start_urls = [
        'https://www.kvic.cz/ka/Vzdelavani/Programova_nabidka/Aktualni_nabidka_kurzu?KurzyRLV=1_1000000'
    ]

    ORG_ICO = '62330403'

    def parse(self, response):
        org_item = OrganizationItem(
            metadata=ItemMetadata(accessed_date_time=datetime.datetime.utcnow(), source=self.name),
            ico=self.ORG_ICO,
            name='Krajské zařízení pro další vzdělávání pedagogických pracovníků a informační centrum, Nový Jičín, příspěvková organizace',
            web='https://www.kvic.cz/'
        )

        yield org_item

        for url in response.xpath('//*[@class="divListCourseName"]//a/@href'):
            yield scrapy.Request("https://www.kvic.cz"+url.get(), callback=self.parse_course_page)

    def parse_course_page(self, response):
        title = response.xpath('//*[@id="MainContent_nazevLit"]/text()').get().strip()
        target_groups = response.xpath('//*[@id="divCourseDetail"]/div/div[2]/text()[2]').get().strip().split(', ')
        description = '\n'.join(response.xpath('//*[@class="kurzInfo"]//p/node()').getall())

        # if the Login button is present
        is_available_for_registration = response.xpath("//span[text()='Přihlásit se']") is not None

        keys = [n.strip() for n in response.xpath("//table[@class='kurzBordered']//td[@class='kurzLeftHeader']/text()").getall()]
        values = [n.strip() for n in response.xpath("//table[@class='kurzBordered']//td[@class='kurzRightContent']//text()").getall() if n.strip() != ""]
        details = dict(zip(keys, values))

        # the first location occurrence in 'sezeni' table
        location = response.xpath("//table[@class='kurz']//tr[2]/td[4]/text()").get().strip()

        # list of all lecturers in the course - from all sittings
        lecturers = set([n.strip() for n in response.xpath("//table[@class='kurz']//tr[position() > 1]//td[5]/text()").getall()])

        course_item = CourseItem(
            metadata=ItemMetadata(accessed_date_time=datetime.datetime.utcnow(), source=self.name),
            org_ico=self.ORG_ICO,
            provider_id=details['Katalogové číslo'],
            accreditation_number=details['Číslo akreditace'],
            url=response.url,
            title=title,
            description=description,
            start_date=details['Začátek kurzu'],
            location=location,
            lecturer=', '.join(lecturers),
            target_groups=target_groups,
            total_duration=details['Rozsah'],
            price=details['Cena kurzu'],
            available_for_registration=is_available_for_registration,
        )
        yield course_item